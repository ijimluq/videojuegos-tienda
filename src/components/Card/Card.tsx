import { Link } from "react-router-dom";
import { directories } from "utilities/consts";
import { printPrice, textAbstract } from "utilities/games";
import { ICard } from "utilities/interfaces";
import "./Card.css";

export const Card = (props: ICard) => {
  const { game } = props;

  return (
    <li>
      <div className="card">
        <Link to={`/info-game/${game.id}`}>
          <img
            src={`${directories.assets}\\${game.image}`}
            alt={`${game.name}`}
            className="cardImage"
          />
        </Link>
        <div className="textCard">
          <div className="infoCard">
            <span className="w100 dIB cardTitle">
              {textAbstract(game.name, 23)}
            </span>
            <span className="w100 dIB cardMoney">
              {printPrice(game.price)} USD
            </span>
          </div>
          <div className="buttonCard">
            <button className="textButton btn">BUY NOW</button>
          </div>
        </div>
      </div>
    </li>
  );
};
