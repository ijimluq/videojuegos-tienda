import { InfoGameContainer } from "components/InfoGameContainer/InfoGameContainer";
import { InfoGameHeader } from "components/InfoGameHeader/InfoGameHeader";
import { useContext } from "react";
import { useParams } from "react-router-dom";
import { AuthContext } from "utilities/AuthContext";
import { IInfoGame } from "utilities/interfaces";
import { TGame } from "utilities/types";
import "./InfoGame.css";

export const InfoGame = (props: IInfoGame) => {
  const { id } = useParams<{ id: string }>();
  const { games } = useContext<{ games: TGame[] }>(AuthContext);

  const selectedGame = games.find((game) =>
    game.id === parseInt(id) ? game : ""
  );

  return (
    <>
      <InfoGameHeader selectedGame={selectedGame} />
      <InfoGameContainer />
    </>
  );
};
