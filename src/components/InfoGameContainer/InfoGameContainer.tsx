import { directories } from "utilities/consts";
import "./InfoGameContainer.css";

export const InfoGameContainer = () => {
  return (
    <section className="infoContainer">
      <div className="imgInitialContainer">
        <img
          src={`\\${directories.assets}\\godofwar-wpp-jpg-423682103.png`}
          alt={"Jueguito"}
          className="imgInitial"
        />
      </div>
      <div className="textGameDesc">
        <p className="description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
          consectetur aliquam malesuada. Fusce eget rhoncus arcu. Fusce
          tincidunt dapibus scelerisque. Pellentesque habitant morbi tristique
          senectus et netus et malesuada fames ac turpis egestas. Nunc blandit
          est arcu, ac porta tortor mattis in. Suspendisse nisl mauris,
          sollicitudin id turpis et, maximus eleifend nibh. Sed non fringilla
          sapien. Mauris tempor massa et urna commodo, in finibus felis viverra.
          Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam
          consectetur sed erat nec sollicitudin. Pellentesque sem risus, sodales
          non nibh ut, dignissim pulvinar augue. Nullam tempus convallis ligula
          quis viverra. Interdum et malesuada fames ac ante ipsum primis in
          faucibus. Sed feugiat placerat augue. Aenean at ligula elit. Aenean
          vel lorem dolor.
        </p>
        <ul className="listProperties">
          <li>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
            consequat est arcu, quis vulputate massa luctus sit amet. Cras elit
            sapien, congue et maximus id, volutpat id nibh. Praesent a.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
            consequat est arcu, quis vulputate massa luctus sit amet. Cras elit
            sapien, congue et maximus id, volutpat id nibh. Praesent a.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
            consequat est arcu, quis vulputate massa luctus sit amet. Cras elit
            sapien, congue et maximus id, volutpat id nibh. Praesent a.
          </li>
        </ul>
      </div>
    </section>
  );
};
