import { useHistory } from "react-router";
import { directories } from "utilities/consts";
import { printPrice } from "utilities/games";
import { IInfoGameHeader } from "utilities/interfaces";
import "./InfoGameHeader.css";

export const InfoGameHeader = (props: IInfoGameHeader) => {
  const { selectedGame } = props;
  const history = useHistory();

  return (
    <div className="infoHeader">
      <div className="relative">
        <img
          src={`..\\${directories.assets}\\god-of-war-2018319161748-10.png`}
          alt={`${selectedGame?.name}`}
          className="imgBackground"
        />
      </div>
      <header className="header">
        <img
          className="iconnavigationchevron_left_24px"
          src={`..\\${directories.assets}\\icon-navigation-chevron-left-24-px.svg`}
          alt="icono back"
          onClick={() => history.goBack()}
        />
        <div className="status">
          <div className="Oval" />

          <span className="newRelease">NEW RELEASE</span>
        </div>
        <div className="gameTitle">{selectedGame?.name}</div>
        <div className="w100 priceLabelContainer">
          <div className="priceLabel">
            <div className="price">
              {typeof selectedGame !== "undefined"
                ? printPrice(selectedGame.price)
                : "Error"}
              $
            </div>
            <div>
              <button className="btn textButton installButton">
                INSTALL GAME
              </button>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
};
