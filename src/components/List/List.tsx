import { useContext } from "react";
import { AuthContext } from "utilities/AuthContext";
import { IList } from "utilities/interfaces";
import { TGame } from "utilities/types";
import { Card } from "../Card/Card";
import "./List.css";

export const List = (props: IList) => {
  const { games } = useContext(AuthContext);

  return (
    <>
      <div className="row">
        <div className="c12 mx">
          <span className="BESTSELLERS">Bestsellers</span>
        </div>
      </div>
      <div className="row">
        <ul className="main mx">
          {games.map((game: TGame) => (
            <Card key={game.id} game={game} />
          ))}
        </ul>
      </div>
    </>
  );
};
