import { InfoGame } from "components/InfoGame/InfoGame";
import { List } from "components/List/List";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { AuthContextProvider } from "utilities/AuthContextProvider";

import "./App.css";

function App() {
  return (
    <Router>
      <main className="container">
        <AuthContextProvider>
          <Switch>
            <Route exact path="/">
              <List />
            </Route>
            <Route path="/info-game/:id" exact>
              <InfoGame />
            </Route>
            <Route path="/**">
              <Redirect to="/" />
            </Route>
          </Switch>
        </AuthContextProvider>
      </main>
    </Router>
  );
}

export default App;
