import { TGame } from "./types";

export interface IList {}

export interface ICard {
  game: TGame;
}
export interface IInfoGame {}
export interface IInfoGameHeader {
  selectedGame: TGame | undefined;
}

export interface IAuthContextProvider {
  children: JSX.Element;
}

export interface IAuthContext {
  games: TGame[];
}
