import { useCallback, useEffect, useState } from "react";
import { AuthContext } from "./AuthContext";
import { IAuthContextProvider } from "./interfaces";
import { TGame } from "./types";

export const AuthContextProvider = (props: IAuthContextProvider) => {
  const { children } = props;
  const [gamesAPI, setGamesAPI] = useState<Array<TGame>>([]);

  const getGamesApi = useCallback(async (): Promise<void> => {
    const resp = await fetch("http://localhost:3000/games/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (resp.ok) {
      const gamesAPIDummy = await resp.json();
      setGamesAPI([...gamesAPIDummy]);
    }
  }, []);

  useEffect(() => {
    getGamesApi();
  }, [getGamesApi]);
  return (
    <AuthContext.Provider value={{ games: gamesAPI }}>
      {children}
    </AuthContext.Provider>
  );
};
