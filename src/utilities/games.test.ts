import { printPrice, textAbstract } from "./games";

describe("Games", () => {
  it("should replace . to ,", () => {
    expect("58,50").toEqual(printPrice(58.5));
  });
  it("should change text to red dead r...", () => {
    expect("red dead r...").toEqual(textAbstract("red dead redemption 2", 10));
  });
});
