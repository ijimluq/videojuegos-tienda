export const printPrice: (price: number | undefined) => string = (price) => {
  if (typeof price === "undefined") {
    return "Error";
  } else {
    return price.toFixed(2).toString().replace(".", ",");
  }
};
export const textAbstract: (text: string, length: number) => string = (
  text,
  length
) => {
  if (text === null) {
    return "";
  }
  if (text.length <= length) {
    return text;
  }
  text = text.substring(0, length);

  // Si tiene espacio final lo elimina
  if (text[length + 1] === " ") {
    text = text.substring(0, length);
  }

  return text + "...";
};
