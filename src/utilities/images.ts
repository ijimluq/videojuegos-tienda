export const getImageForSizeLevel: (
  image: string | undefined,
  level: number
) => string = (image, level) => {
  if (typeof image === "undefined") return "Image is undefined";
  const lastDot = image.lastIndexOf(".");

  return `${image.substring(0, lastDot)}@${level}x${image.substring(
    lastDot,
    image.length
  )}`;
};
