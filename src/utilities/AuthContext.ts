import { IAuthContext } from "./interfaces";
import { createContext } from "react";

export const AuthContext = createContext<IAuthContext>({ games: [] });
